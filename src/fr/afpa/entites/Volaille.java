package fr.afpa.entites;

import java.io.Serializable;

public abstract class Volaille implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 9217616009095946737L;
	private String id;
	private boolean isInElevage;

	public Volaille(String id, boolean isInElevage) {
		super();
		this.id = id;
		this.isInElevage = isInElevage;
	}

	public boolean isInElevage() {
		return isInElevage;
	}

	public void setInElevage(boolean isInElevage) {
		this.isInElevage = isInElevage;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return " Volaille id= " + id + " isInElevage= " + isInElevage + "]";
	}

}
