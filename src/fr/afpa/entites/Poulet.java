package fr.afpa.entites;

public class Poulet extends VolailleAbattable {
	
	private static final long serialVersionUID = 7403841281849483961L;
	public static final int nombreMax = 5;
	private static float prix;
	private static float poidsAbatage;

	public Poulet(String id, boolean isInElevage, float poids) {
		super(id, isInElevage, poids);
	}

	public static float getPrix() {
		return prix;
	}

	public static void setPrix(float prix) {
		Poulet.prix = prix;
	}

	public static float getPoidsAbatage() {
		return poidsAbatage;
	}

	public static void setPoidsAbatage(float poidsAbatage) {
		Poulet.poidsAbatage = poidsAbatage;
	}

	public static int getNombremax() {
		return nombreMax;
	}

	@Override
	public String toString() {
		return "Poulet [prix= " + prix + " poidsAbatage= " + poidsAbatage + super.toString();
	}

}
