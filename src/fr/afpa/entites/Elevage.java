package fr.afpa.entites;

import java.io.Serializable;
import java.util.ArrayList;

public class Elevage implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6698292788223380711L;
	ArrayList<Volaille> volailles;
	public static final int nombreMax = 7;

	public Elevage() {
		super();
		this.volailles = new ArrayList<Volaille>();
	}

	public ArrayList<Volaille> getVolailles() {
		return volailles;
	}

	public void setVolailles(ArrayList<Volaille> volailles) {
		this.volailles = volailles;
	}

}
