package fr.afpa.entites;

public class Canard extends VolailleAbattable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 7536753744824949232L;
	public static final int nombreMax = 4;
	private static float prix;
	private static float poidsAbatage;

	public Canard(String id, boolean isInElevage, float poids) {
		super(id, isInElevage, poids);
		// TODO Auto-generated constructor stub
	}

	public static float getPrix() {
		return prix;
	}

	public static void setPrix(float prix) {
		Canard.prix = prix;
	}

	public static float getPoidsAbatage() {
		return poidsAbatage;
	}

	public static void setPoidsAbatage(float poidsAbatage) {
		Canard.poidsAbatage = poidsAbatage;
	}

	public static int getNombremax() {
		return nombreMax;
	}

	@Override
	public String toString() {
		return "Canard [prix= " + prix + " poidsAbatage= " + poidsAbatage + super.toString();
	}

}
