package fr.afpa.entites;

public abstract class VolailleAbattable extends Volaille {
	/**
	 * 
	 */
	private static final long serialVersionUID = -6765793585048480708L;
	private float poids;

	public VolailleAbattable(String id, boolean isInElevage, float poids) {
		super(id, isInElevage);
		this.poids = poids;
	}

	public float getPoids() {
		return poids;
	}

	public void setPoids(float poids) {
		this.poids = poids;
	}

	@Override
	public String toString() {
		return " VolailleAbattable poids= " + poids + super.toString();
	}

}
