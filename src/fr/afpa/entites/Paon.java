package fr.afpa.entites;

public final class Paon extends Volaille {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int nombreMax = 3;

	public Paon(String id, boolean isInElevage) {
		super(id, isInElevage);
	}

	public static int getNombremax() {
		return nombreMax;
	}

	@Override
	public String toString() {
		return "Paon [" + super.toString();
	}

}
