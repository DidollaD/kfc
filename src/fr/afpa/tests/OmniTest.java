package fr.afpa.tests;

import fr.afpa.tests.control.TestControlCanard;
import fr.afpa.tests.control.TestControlElevage;
import fr.afpa.tests.control.TestControlPaon;
import fr.afpa.tests.control.TestControlPoulet;
import fr.afpa.tests.service.TestServiceElevage;
import fr.afpa.tests.service.TestServiceRecherche;
import fr.afpa.tests.service.TestServiceVolaille;
import junit.extensions.ActiveTestSuite;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import junit.textui.TestRunner;

public class OmniTest extends TestCase {
	public static TestSuite suite() {
		TestSuite suite = new ActiveTestSuite();
		suite.addTestSuite(TestControlElevage.class);
		suite.addTestSuite(TestControlCanard.class);
		suite.addTestSuite(TestControlPoulet.class);
		suite.addTestSuite(TestControlPaon.class);
		suite.addTestSuite(TestServiceElevage.class);
		suite.addTestSuite(TestServiceVolaille.class);
		suite.addTestSuite(TestServiceRecherche.class);
		return suite;
	}

	public static void main(String[] args) {
		TestRunner.run(suite());
	}
}
