package fr.afpa.tests.service;

import org.junit.Test;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.services.ServiceCanard;
import fr.afpa.services.ServicePaon;
import fr.afpa.services.ServicePoulet;
import junit.framework.TestCase;

public class TestServiceVolaille extends TestCase {
	private Elevage testKfc;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		testKfc = new Elevage();
		testKfc.getVolailles().add(new Poulet("24601", true, (float) 2.0));
		testKfc.getVolailles().add(new Poulet("24602", true, (float) 1.9));
		testKfc.getVolailles().add(new Poulet("24603", false, (float) 2.0));
		testKfc.getVolailles().add(new Poulet("24604", true, (float) 3.0));
		testKfc.getVolailles().add(new Canard("24605", true, (float) 2.9));
		testKfc.getVolailles().add(new Canard("24606", true, (float) 2.9));
		testKfc.getVolailles().add(new Canard("24607", false, (float) 3.0));
		testKfc.getVolailles().add(new Paon("24608", true));
//		Poulet.setPoidsAbatage((float) 2.0);
//		Poulet.setPrix(1);
//		Canard.setPoidsAbatage((float) 3.0);
//		Canard.setPrix(2);
	}

	@Test
	public void testNbrVolailleEleve() {
		assertEquals("Resultat d'addition inattendu", 3, ServicePoulet.NbrVolailleEleve(testKfc));
		assertEquals("Resultat d'addition inattendu", 2, ServiceCanard.NbrVolailleEleve(testKfc));
		assertEquals("Resultat d'addition inattendu", 1, ServicePaon.NbrVolailleEleve(testKfc));
	}

	@Test
	public void ModifPrix() {
		assertTrue("Resultat d'addition inattendu", ServicePoulet.ModifPrix(2));
		assertTrue("Resultat d'addition inattendu", ServiceCanard.ModifPrix(2));
	}

	@Test
	public void ModifPoidsAbattage() {
		assertTrue("Resultat d'addition inattendu", ServicePoulet.ModifPoidsAbattage(2));
		assertTrue("Resultat d'addition inattendu", ServiceCanard.ModifPoidsAbattage(2));
	}

	@Test
	public void ModifPoids() {
		assertTrue("Resultat d'addition inattendu",
				ServicePoulet.ModifPoids((Poulet) testKfc.getVolailles().get(0), 2));
		assertTrue("Resultat d'addition inattendu",
				ServiceCanard.ModifPoids((Canard) testKfc.getVolailles().get(4), 2));
	}

	@Test
	public void VendreVolaille() {
		assertTrue("Resultat d'addition inattendu",
				ServicePoulet.VendreVolaille((Poulet) testKfc.getVolailles().get(0)));
		assertTrue("Resultat d'addition inattendu",
				ServiceCanard.VendreVolaille((Canard) testKfc.getVolailles().get(4)));
		assertTrue("Resultat d'addition inattendu", ServicePaon.RendreVolaille((Paon) testKfc.getVolailles().get(7)));
	}
}
