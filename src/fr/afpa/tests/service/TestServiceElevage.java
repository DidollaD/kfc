
package fr.afpa.tests.service;

import org.junit.Test;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.services.ServiceElevage;
import junit.framework.TestCase;

public class TestServiceElevage extends TestCase {
	private Elevage testKfc;
	private float testTotal;

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		testKfc = new Elevage();
		testKfc.getVolailles().add(new Poulet("24601", true, (float) 2.0));
		testKfc.getVolailles().add(new Poulet("24602", true, (float) 1.9));
		testKfc.getVolailles().add(new Poulet("24603", false, (float) 2.0));
		testKfc.getVolailles().add(new Canard("24604", true, (float) 3.0));
		testKfc.getVolailles().add(new Canard("24605", true, (float) 2.9));
		testKfc.getVolailles().add(new Canard("24607", false, (float) 3.0));
		testKfc.getVolailles().add(new Paon("24606", true));
		Poulet.setPoidsAbatage((float) 2.0);
		Poulet.setPrix(1);
		Canard.setPoidsAbatage((float) 3.0);
		Canard.setPrix(2);
		testTotal = 8;
	}

	@Test
	public void testPrixTotalVolailleAbattable() {
		assertEquals("Resultat d'addition inattendu", testTotal, ServiceElevage.PrixTotalVolailleAbattable(testKfc));
	}
}
