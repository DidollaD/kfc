package fr.afpa.tests.service;

import org.junit.Test;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.services.ServiceElevage;
import fr.afpa.services.ServicePaon;
import junit.framework.TestCase;

public class TestServiceRecherche extends TestCase {
	private Elevage testKfc;
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		testKfc = new Elevage();
		testKfc.getVolailles().add(new Poulet("24601", true, (float) 2.0));
		testKfc.getVolailles().add(new Poulet("24602", true, (float) 1.9));
		testKfc.getVolailles().add(new Poulet("24603", false, (float) 2.0));
		testKfc.getVolailles().add(new Canard("24604", true, (float) 3.0));
		testKfc.getVolailles().add(new Canard("24605", true, (float) 2.9));
		testKfc.getVolailles().add(new Canard("24606", false, (float) 3.0));
		testKfc.getVolailles().add(new Paon("24607", true));
		testKfc.getVolailles().add(new Paon("24608", false));
	}
	
	@Test
	public void testRechercheVolaille() {
		assertNotNull("Resultat d'addition inattendu", ServiceElevage.RechercheVolaille(testKfc, "24601"));
		assertNull("Resultat d'addition inattendu", ServiceElevage.RechercheVolaille(testKfc, "24603"));
		assertNull("Resultat d'addition inattendu", ServiceElevage.RechercheVolaille(testKfc, "0"));
		assertNotNull("Resultat d'addition inattendu", ServicePaon.RechercheVolaille(testKfc, "24607"));
		assertNull("Resultat d'addition inattendu", ServicePaon.RechercheVolaille(testKfc, "24606"));
		assertNull("Resultat d'addition inattendu", ServicePaon.RechercheVolaille(testKfc, "24608"));
		assertNull("Resultat d'addition inattendu", ServicePaon.RechercheVolaille(testKfc, "0"));
	}
}
