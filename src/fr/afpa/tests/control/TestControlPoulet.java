package fr.afpa.tests.control;

import org.junit.Test;

import fr.afpa.controles.ControlePoulet;
import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import junit.framework.TestCase;

public class TestControlPoulet extends TestCase{
private Elevage testKfc;
	
	@Override
	protected void setUp() throws Exception {
		super.setUp();
		testKfc = new Elevage();
		testKfc.getVolailles().add(new Poulet("24601", true, (float) 2.0));
		testKfc.getVolailles().add(new Poulet("24602", true, (float) 2.0));
		testKfc.getVolailles().add(new Poulet("24603", true, (float) 2.0));
		testKfc.getVolailles().add(new Poulet("24604", true, (float) 2.0));
		testKfc.getVolailles().add(new Poulet("24605", true, (float) 2.0));
		testKfc.getVolailles().add(new Canard("24607", true, (float) 2.0));
		testKfc.getVolailles().add(new Paon("24606", true));
	}
	
	@Test
	public void testIsElevageComplet() {
		assertTrue("Resultat d'addition inattendu", ControlePoulet.IsElevageComplet(testKfc));
	}
}
