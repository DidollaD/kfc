package fr.afpa.controles;

import fr.afpa.entites.Elevage;
import fr.afpa.entites.Paon;
import fr.afpa.services.ServicePaon;

public class ControlePaon {
	
	/**
	 * control si le nombre maximum de paon est ateint
	 * 
	 * @param kfc
	 * @return
	 */
	public static boolean IsElevageComplet(Elevage kfc) {
		if (ServicePaon.NbrVolailleEleve(kfc) >= Paon.getNombremax())
			return true;
		return false;
	}
}
