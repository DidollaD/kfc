package fr.afpa.controles;

import fr.afpa.entites.Elevage;
import fr.afpa.entites.Volaille;

public class ControleElevage {
	
	/**
	 * control si le nombre maximum de volaille est ateint
	 * @param kfc
	 * @return
	 */
	public static boolean IsElevageComplet(Elevage kfc) {
		int count = 0;
		for (Volaille volaille : kfc.getVolailles()) {
			if (volaille.isInElevage()) {
				count++;
			}
		}
		if (count >= Elevage.nombreMax)
			return true;
		return false;
	}

	/**
	 * control si l'id entrer est valide
	 * @param kfc
	 * @param idEntrant
	 * @return
	 */
	public static boolean ValideId(Elevage kfc, String idEntrant) {
		for (Volaille volaille : kfc.getVolailles()) {
			if (idEntrant.contentEquals(volaille.getId())) {
				return false;
			}
		}
		return idEntrant.matches("\\d{5}");
	}
}
