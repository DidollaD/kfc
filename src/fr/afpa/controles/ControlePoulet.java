package fr.afpa.controles;

import fr.afpa.entites.Elevage;
import fr.afpa.entites.Poulet;
import fr.afpa.services.ServicePoulet;

public class ControlePoulet {
	
	/**
	 * control si le nombre maximum de poulet est ateint
	 * @param kfc
	 * @return
	 */
	public static boolean IsElevageComplet(Elevage kfc) {
		if (ServicePoulet.NbrVolailleEleve(kfc) >= Poulet.getNombremax())
			return true;
		return false;
	}
}
