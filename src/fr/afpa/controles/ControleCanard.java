package fr.afpa.controles;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.services.ServiceCanard;

public class ControleCanard {
	
	/**
	 * control si le nombre maximum de canard est ateint
	 * @param kfc
	 * @return
	 */
	public static boolean IsElevageComplet(Elevage kfc) {
		if (ServiceCanard.NbrVolailleEleve(kfc) >= Canard.getNombremax())
			return true;
		return false;
	}
}
