package fr.afpa.services;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Volaille;

public class ServiceCanard {
	
	/**
	 * Ajouter un canard dans l'elevage
	 * @param kfc
	 * @param id
	 * @param poids
	 */
	public static void AjoutVolaille(Elevage kfc, String id, float poids) {
		kfc.getVolailles().add(new Canard(id, true, poids));
	}

	/**
	 * retourne le nombre de canard
	 * @param kfc
	 * @return
	 */
	public static int NbrVolailleEleve(Elevage kfc) {
		int count = 0;
		for (Volaille volaille : kfc.getVolailles()) {
			if (volaille.isInElevage() && volaille instanceof Canard) {
				count++;
			}
		}
		return count;
	}

	/**
	 * modifie le poids d'abattage des canards
	 * @param poidsEntrant
	 * @return
	 */
	public static boolean ModifPoidsAbattage(float poidsEntrant) {
		Canard.setPoidsAbatage(poidsEntrant);
		return true;
	}

	/**
	 * modifie le prix du jour des canards
	 * @param prixEntrant
	 * @return
	 */
	public static boolean ModifPrix(float prixEntrant) {
		Canard.setPrix(prixEntrant);
		return true;
	}
	
	/**
	 * modifie le poids d'un canard
	 * @param canard
	 * @param poidsEntrant
	 * @return
	 */
	public static boolean ModifPoids(Canard canard, float poidsEntrant) {
		canard.setPoids(poidsEntrant);
		return true;
	}

	/**
	 * desactive le canard
	 * @param volailleTrouvee
	 * @return
	 */
	public static boolean VendreVolaille(Canard volailleTrouvee) {
		volailleTrouvee.setInElevage(false);
		return true;
	}
}
