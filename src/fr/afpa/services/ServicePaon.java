package fr.afpa.services;

import fr.afpa.entites.Elevage;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Volaille;

public class ServicePaon {
	/**
	 * Ajouter un paon dans l'elevage
	 * @param kfc
	 * @param id
	 * @param poids
	 */
	public static void AjoutVolaille(Elevage kfc, String id) {
		kfc.getVolailles().add(new Paon(id, true));
	}

	public static int NbrVolailleEleve(Elevage kfc) {
		int count = 0;
		for (Volaille volaille : kfc.getVolailles()) {
			if (volaille.isInElevage() && volaille instanceof Paon) {
				count++;
			}
		}
		return count;
	}

	/**
	 * retourne le nombre de paon
	 * @param kfc
	 * @return
	 */
	public static Paon RechercheVolaille(Elevage kfc, String choixId) {
		for (Volaille volaille : kfc.getVolailles()) {
			if (choixId.equals(volaille.getId()) && volaille.isInElevage() && volaille instanceof Paon)
				return (Paon) volaille;
		}
		return null;
	}

	/**
	 * desactive le paon
	 * @param volailleTrouvee
	 * @return
	 */
	public static boolean RendreVolaille(Paon volailleTrouvee) {
		volailleTrouvee.setInElevage(false);
		return true;
	}
}
