package fr.afpa.services.sauvegarde;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Poulet;

public class ServiceSauvgarde {
	public static void Sauvegarde(Elevage kfc) {
		try {
			ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(".\\save\\kfc.save"));
			oos.writeObject(kfc);
			oos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static Elevage LireSauvegarde() {
		Elevage kfc = null;
		try {
			ObjectInputStream ois = new ObjectInputStream(new FileInputStream(".\\save\\kfc.save"));
			try {
				while (true) {
					kfc = (Elevage) ois.readObject();
				}
			} catch (EOFException ex) {

			}
			ois.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return kfc;
	}

	public static void SauvegardeVfs() {
		try {
			DataOutputStream dataOutput = new DataOutputStream(new FileOutputStream(".\\save\\vfs.save"));
			dataOutput.writeFloat(Poulet.getPoidsAbatage());
			dataOutput.writeFloat(Poulet.getPrix());
			dataOutput.writeFloat(Canard.getPoidsAbatage());
			dataOutput.writeFloat(Canard.getPrix());
			dataOutput.close();
		} catch (Exception e) {
			// TODO: handle exception
		}

	}

	public static void LireSauvegardeVfs() {

		try {
			DataInputStream dataInput = new DataInputStream(new FileInputStream(".\\save\\vfs.save"));
			try {
				while (true) {
					Poulet.setPoidsAbatage(dataInput.readFloat());
					Poulet.setPrix(dataInput.readFloat());
					Canard.setPoidsAbatage(dataInput.readFloat());
					Canard.setPrix(dataInput.readFloat());
				}
			} catch (EOFException ex) {

			}
			dataInput.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
