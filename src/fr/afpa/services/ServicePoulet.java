package fr.afpa.services;

import fr.afpa.entites.Elevage;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;

public class ServicePoulet {
	
	/**
	 * Ajouter un poulet dans l'elevage
	 * @param kfc
	 * @param id
	 * @param poids
	 */
	public static void AjoutVolaille(Elevage kfc, String id, float poids) {
		kfc.getVolailles().add(new Poulet(id, true, poids));
	}

	/**
	 * retourne le nombre de poulet
	 * @param kfc
	 * @return
	 */
	public static int NbrVolailleEleve(Elevage kfc) {
		int count = 0;
		for (Volaille volaille : kfc.getVolailles()) {
			if (volaille.isInElevage() && volaille instanceof Poulet) {
				count++;
			}
		}
		return count;
	}

	/**
	 * modifie le poids d'abattage des poulets
	 * @param poidsEntrant
	 * @return
	 */
	public static boolean ModifPoidsAbattage(float poidsEntrant) {
		Poulet.setPoidsAbatage(poidsEntrant);
		return true;
	}

	/**
	 * modifie le prix du jour des poulets
	 * @param prixEntrant
	 * @return
	 */
	public static boolean ModifPrix(float prixEntrant) {
		Poulet.setPrix(prixEntrant);
		return true;
	}
	
	/**
	 * modifie le poids d'un poulet
	 * @param canard
	 * @param poidsEntrant
	 * @return
	 */
	public static boolean ModifPoids(Poulet poulet, float poidsEntrant) {
		poulet.setPoids(poidsEntrant);
		return true;
	}

	/**
	 * desactive le poulet
	 * @param volailleTrouvee
	 * @return
	 */
	public static boolean VendreVolaille(Poulet volailleTrouvee) {
		volailleTrouvee.setInElevage(false);
		return true;
	}
}
