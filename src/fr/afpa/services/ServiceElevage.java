package fr.afpa.services;

import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;

public class ServiceElevage {
	
	/**
	 * Recherche une volaille par son identifiant
	 * @param kfc
	 * @param choixId
	 * @return
	 */
	public static Volaille RechercheVolaille(Elevage kfc, String choixId) {
		for (Volaille volaille : kfc.getVolailles()) {
			if (choixId.equals(volaille.getId()) && volaille.isInElevage() && !(volaille instanceof Paon))
				return volaille;
		}
		return null;
	}

	/**
	 * Calcul le prix total des volaille abattable
	 * @param kfc
	 * @return
	 */
	public static float PrixTotalVolailleAbattable(Elevage kfc) {
		float total = 0;
		for (Volaille volaille : kfc.getVolailles()) {
			if (volaille.isInElevage()) {
				if (volaille instanceof Canard && ((Canard) volaille).getPoids() >= Canard.getPoidsAbatage()) {
					total += ((Canard) volaille).getPoids() * Canard.getPrix();
				} else if (volaille instanceof Poulet && ((Poulet) volaille).getPoids() >= Poulet.getPoidsAbatage()) {
					total += ((Poulet) volaille).getPoids() * Poulet.getPrix();
				}
			}
		}
		return total;
	}
}
