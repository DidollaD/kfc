package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.controles.ControleCanard;
import fr.afpa.controles.ControleElevage;
import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.services.ServiceCanard;

public class AffichageCanard {
	
	/**
	 * Demande les attribut de la volaille a ajoute
	 * @param sc
	 * @param kfc
	 * @return
	 */
	public static boolean AffichageAjoutVolaille(Scanner sc, Elevage kfc) {
		String idEntrant = "";
		float poidsEntrant = 0;
		if (ControleCanard.IsElevageComplet(kfc)) {
			System.out.println("D�j� le nombre maximal de canards");
			return false;
		}
		System.out.println("Entrer le numero de la bague du canards");
		idEntrant = sc.next();
		sc.nextLine();
		if (!ControleElevage.ValideId(kfc, idEntrant)) {
			System.out.println("Saisie invalide");
			return false;
		}
		System.out.println("Entrer le poids du canard");
		try {
			poidsEntrant = sc.nextFloat();
			sc.nextLine();
		} catch (Exception e) {
			sc.nextLine();
			System.out.println("Saisie invalide");
			return false;
		}
		ServiceCanard.AjoutVolaille(kfc, idEntrant, poidsEntrant);
		return true;
	}

	/**
	 * Demande le nouveau poids d'abattage
	 * @param sc
	 * @return
	 */
	public static boolean AffichageModifPoidsAbattage(Scanner sc) {
		float poidsEntrant = 0;
		System.out.println("Entrer le nouveau poids d'abattage");
		try {
			poidsEntrant = sc.nextFloat();
			sc.nextLine();
		} catch (Exception e) {
			sc.nextLine();
			System.out.println("Saisie invalide");
			return false;
		}
		ServiceCanard.ModifPoidsAbattage(poidsEntrant);
		return true;
	}

	/**
	 * Demande le nouveau prix du jour
	 * @param sc
	 * @return
	 */
	public static boolean AffichageModifPrix(Scanner sc) {
		float prixEntrant = 0;
		System.out.println("Entrer le nouveau prix au kilo");
		try {
			prixEntrant = sc.nextFloat();
			sc.nextLine();
		} catch (Exception e) {
			sc.nextLine();
			System.out.println("Saisie invalide");
			return false;
		}
		ServiceCanard.ModifPrix(prixEntrant);
		return true;
	}

	/**
	 * Demande le nouveau poids du canard
	 * @param canard
	 * @param sc
	 * @return
	 */
	public static boolean AffichageModifPoids(Canard canard, Scanner sc) {
		float poidsEntrant = 0;
		System.out.println("Entrer le nouveau poids du canard");
		try {
			poidsEntrant = sc.nextFloat();
			sc.nextLine();
		} catch (Exception e) {
			sc.nextLine();
			System.out.println("Saisie invalide");
			return false;
		}
		ServiceCanard.ModifPoids(canard, poidsEntrant);
		return true;
	}

}
