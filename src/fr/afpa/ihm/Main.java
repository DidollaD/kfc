package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.entites.Elevage;
import fr.afpa.entites.Volaille;
import fr.afpa.services.sauvegarde.ServiceSauvgarde;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Elevage kfc = ServiceSauvgarde.LireSauvegarde();
		ServiceSauvgarde.LireSauvegardeVfs();
		char choix = 'a';
		do {
			AffichageMenu.Menu();
			choix = AffichageMenu.ApplicationChoix(sc, kfc);
			ServiceSauvgarde.Sauvegarde(kfc);
		} while (choix != '0');
		for (Volaille volaille : kfc.getVolailles()) {
			System.out.println(volaille.toString());
		}
		sc.close();
	}

}
