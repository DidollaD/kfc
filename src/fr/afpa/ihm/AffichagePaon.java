package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.controles.ControleElevage;
import fr.afpa.controles.ControlePaon;
import fr.afpa.entites.Elevage;
import fr.afpa.services.ServicePaon;

public class AffichagePaon {
	
	/**
	 * Demande les attribut de la volaille a ajoute
	 * @param sc
	 * @param kfc
	 * @return
	 */
	public static boolean AffichageAjoutVolaille(Scanner sc, Elevage kfc) {
		String idEntrant = "";
		if (ControlePaon.IsElevageComplet(kfc)) {
			System.out.println("D�j� le nombre maximal de paons");
			return false;
		}
		System.out.println("Entrer le numero de la bague du paons");
		idEntrant = sc.next();
		sc.nextLine();
		if (!ControleElevage.ValideId(kfc, idEntrant)) {
			System.out.println("Saisie invalide");
			return false;
		}
		ServicePaon.AjoutVolaille(kfc, idEntrant);
		return true;
	}
}
