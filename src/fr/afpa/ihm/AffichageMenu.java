package fr.afpa.ihm;

import java.util.Scanner;

import fr.afpa.controles.ControleElevage;
import fr.afpa.entites.Canard;
import fr.afpa.entites.Elevage;
import fr.afpa.entites.Paon;
import fr.afpa.entites.Poulet;
import fr.afpa.entites.Volaille;
import fr.afpa.services.ServiceCanard;
import fr.afpa.services.ServiceElevage;
import fr.afpa.services.ServicePaon;
import fr.afpa.services.ServicePoulet;
import fr.afpa.services.sauvegarde.ServiceSauvgarde;

public class AffichageMenu {
	private static char choix;
	private static String choixId;

	/**
	 * Affiche le menu 
	 * 
	*/
	public static void Menu() {
		System.out.println("" + "1 - ajouter une volaille\r\n" + "2 - modifier poids abattage\r\n"
				+ "3 - modifier prix du jour\r\n" + "4 - modifier poids d'une volaille\r\n"
				+ "5 - voir le nombre de volailles par type\r\n"
				+ "6 - voir le total de prix des volailles abattables\r\n" + "7 - vendre une volaille.\r\n"
				+ "8 - rendre un paon au parc.\r\n\n" + "0 - Quitter\r\n\n" + "Votre choix");
	}
	
	
	/**
	 * Appel les methode selon le choix de l'utilisateur
	 * @param sc
	 * @param kfc
	 * @return
	 */
	public static char ApplicationChoix(Scanner sc, Elevage kfc) {
		choix = sc.next().charAt(0);
		sc.nextLine();
		switch (choix) {
		case '1':
			AffichageAjout(sc, kfc);
			break;
		case '2':
			ModifPoidsAbattage(sc);
			break;
		case '3':
			ModifPrix(sc);
			break;
		case '4':
			ModifPoids(sc, kfc);
			break;
		case '5':
			AfficherNbrVolailleType(kfc);
			break;
		case '6':
			AfficherPrixTotalVolailleAbattable(kfc);
			break;
		case '7':
			AfficherVenteVolaille(sc, kfc);
			break;
		case '8':
			AfficherRendrePaon(sc, kfc);
			break;
		case '0':
			break;
		default:
			System.out.println("saisie invalide");
			break;
		}
		return choix;
	}
	/**
	 * Demande le type de volaille a ajoute
	 * @param sc
	 * @param kfc
	 */
	private static void AffichageAjout(Scanner sc, Elevage kfc) {
		if (ControleElevage.IsElevageComplet(kfc))
			System.out.println("Elevage compl�te");
		else {
			System.out.println("De quel type de volaille voulez-vous ajout� ?\n" + "1 - Canard\n2 - Poulet\n"
					+ "3 - Paon\n\n" + "0 - Annuler\r\n");
			char choix = sc.next().charAt(0);
			sc.nextLine();
			switch (choix) {
			case '1':
				AffichageCanard.AffichageAjoutVolaille(sc, kfc);
				break;
			case '2':
				AffichagePoulet.AffichageAjoutVolaille(sc, kfc);
				break;
			case '3':
				AffichagePaon.AffichageAjoutVolaille(sc, kfc);
				break;
			case '0':
				break;
			default:
				System.out.println("saisie invalide");
				break;
			}
		}
	}

	/**
	 * Demande le type volaille a laquelle  modifie le poids d'abattage
	 * @param sc
	 */
	private static void ModifPoidsAbattage(Scanner sc) {
		System.out.println(
				"Quel type de volaille voulez-vous modifi� le poids d'abattage ?\n1 - Canard\n2 - Poulet\n\n0 - Annuler\r\n");
		choix = sc.next().charAt(0);
		sc.nextLine();
		switch (choix) {
		case '1':
			AffichageCanard.AffichageModifPoidsAbattage(sc);
			break;
		case '2':
			AffichagePoulet.AffichageModifPoidsAbattage(sc);
			break;
		case '0':
			break;
		default:
			System.out.println("saisie invalide");
			break;
		}
		ServiceSauvgarde.SauvegardeVfs();
	}

	/**
	 * Demande le type volaille a laquelle  modifie le prix
	 * @param sc
	 */
	private static void ModifPrix(Scanner sc) {
		System.out.println(
				"De quel type de volaille voulez-vous modifi� le prix au kilo ?\n1 - Canard\n2 - Poulet\n\n0 - Annuler\r\n");
		choix = sc.next().charAt(0);
		sc.nextLine();
		switch (choix) {
		case '1':
			AffichageCanard.AffichageModifPrix(sc);
			break;
		case '2':
			AffichagePoulet.AffichageModifPrix(sc);
			break;
		case '0':
			break;
		default:
			System.out.println("saisie invalide");
			break;
		}
		ServiceSauvgarde.SauvegardeVfs();
	}

	/**
	 * Demande la volaille a laquelle modifie le poids
	 * @param sc
	 * @param kfc
	 */
	private static void ModifPoids(Scanner sc, Elevage kfc) {
		System.out.println("De quel volaille voulez-vous modifi� le poids ?\n\n0 - Annuler\r\n");
		choixId = sc.next();
		sc.nextLine();
		if (!("0".equals(choixId))) {
			Volaille volailleTrouvee = ServiceElevage.RechercheVolaille(kfc, choixId);
			if (volailleTrouvee instanceof Canard) {
				AffichageCanard.AffichageModifPoids((Canard) volailleTrouvee, sc);
			} else if (volailleTrouvee instanceof Poulet) {
				AffichagePoulet.AffichageModifPoids((Poulet) volailleTrouvee, sc);
			}
		}
	}
	
	/**
	 * Afficher le nombre de volaille de chaques types
	 * @param kfc
	 */
	private static void AfficherNbrVolailleType(Elevage kfc) {
		System.out.println("Volailles :\n" + "Canard :   " + ServiceCanard.NbrVolailleEleve(kfc) + "\n" + "Poulet :   "
				+ ServicePoulet.NbrVolailleEleve(kfc) + "\n" + "Paon   :   " + ServicePaon.NbrVolailleEleve(kfc)
				+ "\n");
	}

	/**
	 * Afficher le prix total de chaque volaille abattable
	 * @param kfc
	 */
	private static void AfficherPrixTotalVolailleAbattable(Elevage kfc) {
		System.out.println(ServiceElevage.PrixTotalVolailleAbattable(kfc));
	}

	/**
	 * Demande la volaille a vendre
	 * @param sc
	 * @param kfc
	 */
	private static void AfficherVenteVolaille(Scanner sc, Elevage kfc) {
		System.out.println("De quel volaille voulez-vous vendre ?\n\n0 - Annuler\r\n");
		choixId = sc.next();
		sc.nextLine();
		if (!("0".equals(choixId))) {
			Volaille volailleTrouvee = ServiceElevage.RechercheVolaille(kfc, choixId);
			if (volailleTrouvee instanceof Canard) {
				ServiceCanard.VendreVolaille((Canard) volailleTrouvee);
			} else if (volailleTrouvee instanceof Poulet) {
				ServicePoulet.VendreVolaille((Poulet) volailleTrouvee);
			} else {
				System.out.println("Volaille introuvable");
			}
		}
	}

	/**
	 * Demande la paon a vendre
	 * @param sc
	 * @param kfc
	 */
	private static void AfficherRendrePaon(Scanner sc, Elevage kfc) {
		System.out.println("De quel volaille voulez-vous vendre ?\n\n0 - Annuler\r\n");
		choixId = sc.next();
		sc.nextLine();
		if (!("0".equals(choixId))) {
			Paon volailleTrouvee = ServicePaon.RechercheVolaille(kfc, choixId);
			if (volailleTrouvee != null) {
				ServicePaon.RendreVolaille(volailleTrouvee);
			} else {
				System.out.println("Volaille introuvable");
			}
		}
	}
}
